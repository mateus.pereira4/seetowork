import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login'
import Cadastro from '@/components/Cadastro'
import Encomendas from '@/components/Encomendas'
import Etapas from '@/components/Etapas'
import Rastreio from '@/components/Rastreio'
import BadGateway from '@/components/BadGateway'
import Cliente from '@/components/Cliente'
import Home from '@/views/Home.vue'


Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/cadastro',
      name: 'Cadastro',
      component: Cadastro
    },
    {
      path: '/encomendas',
      name: 'Encomendas',
      props: { page: 1 },
      component: Encomendas
    },
    {
      path: '/etapas',
      name: 'Etapas',
      props: { page: 2 },
      component: Etapas
    },
    {
      path: '/rastreio',
      name: 'Rastreio',
      props: { page: 3 },
      component: Rastreio
    },
    {
      path: '/badGateway',
      name: 'BadGateway',
      props: { page: 4 },
      component: BadGateway
    },
    {
      path: '/cliente',
      name: 'Cliente',
      props: { page: 5 },
      component: Cliente
    }
  ]
})